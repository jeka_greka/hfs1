//
// Created by evgenii on 29.10.2021.
//

#ifndef LAB1_UTILS_H
#define LAB1_UTILS_H

#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

void generate_numbers(int *numbers, long count);
int **divide_array(const int *array, long size, int count);
void merge_partitions(int *array, long size, int **parts, int parts_count);

#endif //LAB1_UTILS_H
