#include "shell_sort.h"
#include "utils.h"
#include "merge_sort.h"

void test_sorts(long arr_size);

int main() {

    srand(time(NULL));

    test_sorts(100);
    test_sorts(1000);
    test_sorts(10000);
    test_sorts(100000);
    test_sorts(1000000);
    test_sorts(10000000);
    test_sorts(100000000);

    return 0;
}

void test_sorts(long arr_size){
    printf("array size: %ld\n", arr_size);
    struct timeval start, stop;
    int *buf = malloc(arr_size * sizeof(int));
    int *array = malloc(arr_size * sizeof(int));
    int *copy = malloc(arr_size * sizeof(int));
    generate_numbers(array, arr_size);
    memcpy(copy, array, arr_size);

    gettimeofday(&start, NULL);
    shell_sort(array, arr_size);
    gettimeofday(&stop, NULL);
    printf("shell sort: %lu mcs\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

    memcpy(array, copy, arr_size);

    gettimeofday(&start, NULL);
    parallel_shell_sort(array, arr_size);
    gettimeofday(&stop, NULL);
    printf("parallel shell sort: %lu mcs\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

    memcpy(array, copy, arr_size);

    gettimeofday(&start, NULL);
    parallel_shell_sort_partitions(array, arr_size);
    gettimeofday(&stop, NULL);
    printf("parallel shell sort partitions: %lu mcs\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

    memcpy(array, copy, arr_size);

    gettimeofday(&start, NULL);
    merge_sort(array, arr_size, buf);
    gettimeofday(&stop, NULL);
    printf("merge sort: %lu mcs\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

    memcpy(array, copy, arr_size);

    gettimeofday(&start, NULL);
    parallel_merge_sort(array, arr_size, buf);
    gettimeofday(&stop, NULL);
    printf("parallel merge sort: %lu mcs\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

    memcpy(array, copy, arr_size);

    gettimeofday(&start, NULL);
    parallel_merge_sort_partitions(array, arr_size, buf);
    gettimeofday(&stop, NULL);
    printf("parallel merge sort partitions: %lu mcs\n\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

}




