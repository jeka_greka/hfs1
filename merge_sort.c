//
// Created by evgenii on 31.10.2021.
//

#include "merge_sort.h"
#include <omp.h>

void merge_sort(int *array, long size, int *buf) {
    if (size < 2) return;

    merge_sort(array, size / 2, buf);
    merge_sort(&array[size / 2], size - (size / 2), buf + size / 2);

    long i = 0, j = size / 2, k = 0;
    while ((i < size / 2) && (j < size)) {
        if (array[i] < array[j])
            buf[k++] = array[i++];
        else
            buf[k++] = array[j++];
    }
    while (i < size / 2) buf[k++] = array[i++];
    while (j < size) buf[k++] = array[j++];

    for (i = 0; i < size; i++) {
        array[i] = buf[i];
    }
}

void merge_sort_with_tasks(int *array, long size, int *buf) {
    if (size < 2) return;

#pragma omp task default(none) shared(array, size, buf) if(size>1000)
    merge_sort_with_tasks(array, size / 2, buf);

#pragma omp task default(none) shared(array, size, buf) if(size>1000)
    merge_sort_with_tasks(&array[size / 2], size - (size / 2), buf + size / 2);


#pragma omp taskwait
    long i = 0, j = size / 2, k = 0;
    while ((i < size / 2) && (j < size)) {
        if (array[i] < array[j])
            buf[k++] = array[i++];
        else
            buf[k++] = array[j++];
    }
    while (i < size / 2) buf[k++] = array[i++];
    while (j < size) buf[k++] = array[j++];


    for (i = 0; i < size; i++) {
        array[i] = buf[i];
    }
}


void parallel_merge_sort(int *array, long size, int *buf) {
#pragma omp parallel default(none) shared(array, size, buf) num_threads(2)
    {
#pragma omp single
        {
            merge_sort_with_tasks(array, size, buf);
        }
    }
}

void parallel_merge_sort_partitions(int *array, long size, int *buf) {
    int i;
    int parts_count = omp_get_max_threads();
    long part_size = size / parts_count;
    int **parts = divide_array(array, size, parts_count);
#pragma omp parallel default(none) shared(parts, i, part_size, parts_count, size, buf) num_threads(2)
    {
#pragma omp single
        {
            for (i = 0; i < parts_count; ++i) {
#pragma omp task default(none) shared(parts, i, part_size, parts_count, size, buf)
                {
                    if (i != parts_count - 1)
                        merge_sort(parts[i], part_size, buf + part_size * i);
                    else merge_sort(parts[i], part_size + size % parts_count, buf + part_size * i);
                }
            }
        }
    }
#pragma omp taskwait
    merge_partitions(array, size, parts, parts_count);
}




