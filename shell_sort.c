//
// Created by evgenii on 29.10.2021.
//

#include "shell_sort.h"
#include "utils.h"

void shell_sort(int *array, long size) {
    long j, interval;
    int temp;
    for (interval = size / 2; interval > 0; interval = interval / 2) {
        for (long i = 0; i < interval; i++) {
            for (long f = interval + i; f < size; f = f + interval) {
                j = f;
                while (j > i && array[j - interval] > array[j]) {
                    temp = array[j];
                    array[j] = array[j - interval];
                    array[j - interval] = temp;
                    j = j - interval;
                }
            }
        }
    }
}

void parallel_shell_sort(int *array, long size) {
    long j = 0, interval;
    int temp = 0;
    for (interval = size / 2; interval > 0; interval = interval / 2) {
#pragma omp parallel for shared( array, size, interval) private(j, temp)  default(none) num_threads(2)
        for (long i = 0; i < interval; i++) {
            for (long f = interval + i; f < size; f = f + interval) {
                j = f;
                while (j > i && array[j - interval] > array[j]) {
                    temp = array[j];
                    array[j] = array[j - interval];
                    array[j - interval] = temp;
                    j = j - interval;
                }
            }
        }
    }
}


void parallel_shell_sort_partitions(int *array, long size) {
    int i;
    int parts_count = omp_get_max_threads();
    long part_size = size / parts_count;
    int **parts = divide_array(array, size, parts_count);
#pragma omp parallel default(none) shared(parts, i, part_size, parts_count, size) num_threads(2)
    {
#pragma omp single
        {
            for (i = 0; i < parts_count; ++i) {
#pragma omp task default(none) shared(parts, i, part_size, parts_count, size)
                {
                    if (i != parts_count - 1)
                        shell_sort(parts[i], part_size);
                    else shell_sort(parts[i], part_size + size % parts_count);
                }
            }
        }
    }
#pragma omp taskwait
    merge_partitions(array, size, parts, parts_count);
}

