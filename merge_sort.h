//
// Created by evgenii on 31.10.2021.
//

#ifndef LAB1_MERGE_SORT_H
#define LAB1_MERGE_SORT_H
#include "utils.h"


void merge_sort(int *array, long size, int *buf);
void parallel_merge_sort(int *array, long size, int *buf);
void parallel_merge_sort_partitions(int *array, long size, int *buf);
#endif //LAB1_MERGE_SORT_H
