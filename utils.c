//
// Created by evgenii on 29.10.2021.
//

#include "utils.h"
const float RAND_MAX_F = RAND_MAX;

float get_rand() {
    return rand() / RAND_MAX_F;
}

float get_rand_range(const float min, const float max) {
    return get_rand() * (max - min) + min;
}

void generate_numbers(int* numbers, long count) {
    for (long i = 0; i < count; ++i) {
        numbers[i] = get_rand_range(INT_MIN, INT_MAX);
    }
}

void merge_partitions(int *array, long size, int **parts, int parts_count) {
    long part_size = size / parts_count;
    int *indexes = calloc(parts_count, sizeof(int));
    for (long j = 0; j < size; ++j) {
        int min = INT_MAX;
        int index;
        for (
                int k = 0;
                k < parts_count;
                ++k) {
            if (parts[k][indexes[k]] < min) {
                min = parts[k][indexes[k]];
                index = k;
            }
        }
        array[j] = min;
        if (index < parts_count - 1) {
            if (indexes[index] < part_size - 1)
                indexes[index]++;
            else parts[index][indexes[index]] = INT_MAX;
        } else {
            if (indexes[index] < part_size + size % parts_count - 1)
                indexes[index]++;
            else parts[index][indexes[index]] = INT_MAX;
        }
    }
}

int **divide_array(const int *array, long size, int count) {
    long i = 0;
    long part_size = size / count;
    long last_part_size = part_size + size % count;
    int **parts = malloc(sizeof(int *) * count);
    long part_index = 0;
    while (i <= size - part_size) {
        if (i + part_size * 2 >= size) {
            part_size = last_part_size;
        }
        int *part = malloc(sizeof(int) * part_size);
        for (int j = 0; j < part_size; ++j) {
            part[j] = array[i];
            i++;
        }
        parts[part_index] = part;
        part_index++;
    }
    return parts;
}

int check_sort(int *numbers, int size){
    for (int i = 0; i < size - 1; ++i) {
        if (numbers[i + 1] < numbers[i]) {
            return 0;
        }
    }
    return 1;
}
