//
// Created by evgenii on 29.10.2021.
//

#ifndef LAB1_SHELL_SORT_H
#define LAB1_SHELL_SORT_H

#include <omp.h>

void parallel_shell_sort(int *array, long size);

void shell_sort(int *array, long size);
void parallel_shell_sort_partitions(int *array, long size);


#endif //LAB1_SHELL_SORT_H
